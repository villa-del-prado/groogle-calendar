package com.puravida.groogle

enum EventStatus {

    CANCELLED('cancelled'),
    CONFIRMED('confirmed'),
    TENTATIVE('tentative')

    String key

    EventStatus(String key){
        this.key = key
    }

    static EventStatus findByKey(String key){
        values().find{ it.key == key }
    }
}