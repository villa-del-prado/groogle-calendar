package com.puravida.groogle

interface WithEvent {

    String getSummary()

    WithEvent setSummary(String summary)

    String getDescription()
    WithEvent setDescription(String description)

    Date getStart()
    WithEvent setStartDay(Date d)
    WithEvent setStart(Date d)

    Date getEnd()
    WithEvent setEndDay(Date d)
    WithEvent setEnd(Date d)

    WithEvent moveTo(Date d)

    WithEvent moveTo(String date)

    EventStatus getStatus()
}
