package com.puravida.groogle

import com.google.api.services.calendar.CalendarScopes
import spock.lang.Shared
import spock.lang.Specification

class BasicGroogleTest extends Specification{

    @Shared
    Groogle groogle

    void setup(){
        //tag::register[]
        groogle = GroogleBuilder.build {
            withOAuthCredentials {
                applicationName 'test-calendar'
                withScopes CalendarScopes.CALENDAR
                usingCredentials "src/test/resources/client_secret.json"
                storeCredentials true
            }
            service(CalendarServiceBuilder.build(), CalendarService)
        }
        //end::register[]
    }

    def "service registered"(){
        //tag::service[]
        given: "a service"

        CalendarService service = groogle.service(CalendarService)

        expect:
        service
        //end::service[]
    }

    def "list calendars"(){
        //tag::listCalendars[]
        given: "a service"

        CalendarService service = groogle.service(CalendarService)

        expect:
        service.eachCalendarInList {
            println it.summary
        }
        //end::listCalendars[]
    }

    def "with primary Calendar"(){
        //tag::primaryCalendar[]
        when: "a service"
        CalendarService service = groogle.service(CalendarService)

        and:
        service.withPrimaryCalendar{
            println "$it.summary"
        }
        //end::primaryCalendar[]
        then:
        true
    }

    def "with a Calendar"(){

        //tag::withCalendar[]
        when: "a service"
        CalendarService service = groogle.service(CalendarService)

        and:
        service.withCalendar 'groovy.groogle@gmail.com', {
            println "$it.summary $it.timeZone"
        }
        //end::withCalendar[]

        then:
        true
    }

    def "update a Calendar"(){
        when: "a service"
        CalendarService service = groogle.service(CalendarService)

        and:
        String currentTimeZone
        service.withCalendar 'groovy.groogle@gmail.com', {
            currentTimeZone = it.timeZone
        }

        and:
        service.withCalendar 'groovy.groogle@gmail.com', {
            it.timeZone = "Europe/Brussels"
        }

        then:
        service.withCalendar 'groovy.groogle@gmail.com', {
            assert it.timeZone == "Europe/Brussels"
        }

        when:
        service.withCalendar 'groovy.groogle@gmail.com', {
            it.timeZone = currentTimeZone
        }

        then:
        service.withCalendar 'groovy.groogle@gmail.com', {
            assert it.timeZone == currentTimeZone
        }
    }

    def "crud calendar"(){
        // tag::crud[]
        when: "given a service"
        CalendarService service = groogle.service(CalendarService)

        and: "create a calendar"
        String calendarId = service.newCalendar({
            it.summary = "Testing ${new Date()}"
        })

        then:
        assert calendarId

        when: "remove the calendar"
        service.deleteCalendar(calendarId)

        then:
        service.calendarIds.contains(calendarId) == false
        //end::crud[]
    }


}
